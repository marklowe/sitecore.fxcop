﻿namespace Sitecore.FxCop
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Microsoft.FxCop.Sdk;

    internal abstract class SitecoreBaseRule : BaseIntrospectionRule
    {
        public SitecoreBaseRule(string name) : base(name, "Sitecore.FxCop.Rules", typeof(SitecoreBaseRule).Assembly) { }
    }
}